<?php
namespace muzna\contact;

use yii\web\AssetBundle;

class ContactAsset extends AssetBundle
{
 public $sourcePath = '@vendor/muzna/yii2-contact-module/assets';
   
    public $css = [
       'css/main.css',
    ];
    public $js = [
        'js/jquery-3.6.0.min.js',
        'js/submit.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}


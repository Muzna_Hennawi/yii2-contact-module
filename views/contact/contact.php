<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\captcha\Captcha;
use yii\helpers\Url;
use yii\web\View;


$this->title = Yii::t('app', 'Contact');
//$this->params['breadcrumbs'][] = $this->title;
\muzna\contact\ContactAsset::register($this);
?>
<style>
    div.required label:after {
        content: " * ";
    }
    #v:after {
        content: " * ";
    }

</style>
<script src='https://www.google.com/recaptcha/api.js'></script>

<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="container pt-lg-5">
        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>


            <p>
                <?= Yii::$app->getSession()->getFlash('contactFormSubmitted') ?>
            </p>

        <?php else: ?>
            <div>
                <?php $form = ActiveForm::begin(['id' => 'contact-form',
                    'layout' => 'horizontal',
                    'fieldConfig' => [
                        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                        'horizontalCssClasses' => [
                            'label' => 'col-sm-3',
                            'offset' => '',
                            'wrapper' => 'col-sm-9',
                            'error' => '',
                            'hint' => '',
                        ],
                    ],
                ]); ?>
                <?= $form->field($model, 'name')->textInput(['autofocus' => true])->label(Yii::t('app', 'name')) ?>
                <?= $form->field($model, 'email')->label(Yii::t('app', 'email')) ?>

                <?= $form->field($model, 'company')->label(Yii::t('app', 'Company')) ?>
                <?= $form->field($model, 'content')->textarea(['rows' => 6])->label(Yii::t('app', 'Massage Content')) ?>
                <div class="row pb-3 " >
                <div class="col-sm-3"><?= Html::label(Yii::t('app', 'Verification Code'),'',['id'=>'v']) ?></div>

                <div class="col-sm-9"><div class="g-recaptcha" id="verify" data-sitekey="6Lfwpj4fAAAAAJqhRRXc4VATiuSEC80GULANFYp8" aria-required="true"></div></div>

                </div>
                <div class="row">

                    <div class="col-sm-3"></div>
                   <div class="col-sm-9"> <?= Html::submitButton(Yii::t('app', 'Contact as'), ['class' => 'btn btn-info',
                        'name' => 'contact-button',
                        'id' => 'contact-btn',
                        'data-pjax' => '0',
                        'data-url' => Url::toRoute('contact')]) ?></div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>


        <?php endif; ?>
    </div>
</div>
<?php

?>


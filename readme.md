How to install this module:

Step1: First add flowing codes into project `composer.json`

```
"repositories": [
    {
        "type": "git",
        "url": "https://gitlab.com/muzna/yii2-contact-module"
    }
],
```

Then add flowing line to require part of `composer.json` :
```
"muzna/yii2-contact-module": "dev-master",
```

And after that run bellow command in your composer :
```
Composer update muzna/yii2-contact-module
```

Step3: Add flowing lines in your application backend config:

```php
'payment' => [
    'class' => 'muzna\contact\Contact',
    'controllerNamespace' => 'muzna\contact\controllers',
],
```

step4: Migrate the database needed tables by the following:
```
php yii migrate --migrationPath=@vendor/muzna/yii2-contact-module/migrations
```


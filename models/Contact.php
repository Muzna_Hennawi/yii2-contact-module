<?php

namespace muzna\contact\models;

use Carbon\Carbon;
use phpDocumentor\Reflection\Types\This;
use Yii;
use yii\helpers\ArrayHelper;
use DateInterval;
use DatePeriod;

/**
 * This is the model class for table "contact".
 *
 * @property int $c_id
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $place
 * @property string|null $job
 * @property string|null $company
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $fax
 * @property string|null $subject
 * @property string $content
 * @property string|null $hear
 * @property string|null $hear_other
 * @property string $last_update
 */
class Contact extends \yii\db\ActiveRecord
{
    const SUBJECT_ADDS = 0;
    const  SUBJECT_SITE= 1;
    const SUBJECT_QUESTION = 2;
    const SUBJECT_COLLABORATION = 3;
    const SUBJECT_OTHER = 4;

    const HEAR_BY_BROSHOUR=0;
    const HEAR_BY_FAX=1;
    const HEAR_BY_EADD=2;
    const HEAR_BY_NEWSADD=3;
    const HEAR_BY_FRIEND=4;
    const HEAR_BY_SEARCHENGIN=5;
    const HEAR_BY_EMAIL=6;
    const HEAR_BY_PHONE=7;
    const HEAR_BY_VISIT=8;
    const HEAR_BY_OTHER=9;


    public $count;

    public $verifyCode;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'email', 'content'], 'required'],
            [['id'], 'integer'],
            [['content'], 'string'],
            [['name', 'company'], 'string', 'max' => 255],
            ['email','email'],
            [['email'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [

            'id' =>Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'name'),
            'email' => Yii::t('app', 'Email'),
            'company' => Yii::t('app', 'Company'),
            'content' =>Yii::t('app', 'Content'),
        ];
    }
    public function getEmailsCount(){
        $count= Contact::find()->count();
        return $count;
    }

    public function getSubjectList(){
        return [
            self::SUBJECT_ADDS => Yii::t('app', 'Adds with as'),
            self::SUBJECT_SITE => Yii::t('app', 'Comments on Site'),
            self::SUBJECT_QUESTION=>Yii::t('app','Question'),
            self::SUBJECT_COLLABORATION=>Yii::t('app','Collaboration'),
            self::SUBJECT_OTHER=>Yii::t('app','Other')

        ];
    }

    public function getSubjectValue(){

        return ArrayHelper::getValue(self::getSubjectList(),$this->subject);
    }
    public function getHearList(){
        return[
            self::HEAR_BY_BROSHOUR=>Yii::t('cpm','Broshour'),
            self::HEAR_BY_FAX=>Yii::t('cpm','Fax'),
            self::HEAR_BY_EADD=>Yii::t('cpm','Electronic Add'),
            self::HEAR_BY_NEWSADD=>Yii::t('cpm','Newspaper Add'),
            self::HEAR_BY_FRIEND=>Yii::t('cpm','Friend'),
            self::HEAR_BY_SEARCHENGIN=>Yii::t('cpm','Search Engin'),
            self::HEAR_BY_PHONE=>Yii::t('cpm','Phone call'),
            self::HEAR_BY_VISIT=>Yii::t('cpm','Sale Visit'),
            self::HEAR_BY_OTHER=>Yii::t('cpm','Other'),



        ];
    }
    public function getHearValue(){

        return ArrayHelper::getValue(self::getHearList(),$this->hear);
    }
    public function getLast30(){
        $today     = Carbon::today(); // today
        $begin     = $today->sub(new DateInterval('P30D')); //created 30 days interval back
        $end       = Carbon::today();
        $end       = $end->modify('+1 day'); // interval generates upto last day
        $interval  = new DateInterval('P1D'); // 1d interval range
        $daterange = new DatePeriod($begin, $interval, $end); // it always runs forwards in date
        $visits= Contact::find()
            ->andFilterWhere(['in','cast(last_update as date)',$daterange])
            ->select(['last_update','COUNT(*) AS count'])
            ->groupBy('cast(last_update as date)')
            ->all();
        foreach ($visits as $visit ){
            //$os=[$visit->os,$visit->count] ;
            $date=Yii::$app->formatter->asDate($visit->last_update, 'd-M');
            $data[] = ['x' =>$date,'value' =>$visit->count];


        }
        if ($visits) {
            $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['status' => 'success', 'data'=>$data];
            return $result;

        }
        else{
            $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result = ['status' => 'error','msg'=>Yii::t('cpm','there is no information to display')];
            return $result;

        }


    }

    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom($this->email)
                ->setReplyTo([$this->email => $this->name])
                ->setSubject($this->name)
                ->setTextBody($this->content)
                ->send();

            return true;
        }
        return false;
    }
}

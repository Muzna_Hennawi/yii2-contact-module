<?php

namespace muzna\contact\controllers;

namespace muzna\contact\controllers;

use app\components\Helper;
use app\components\HelperE;
use app\models\AnnUsers;
use app\models\Articles;
use app\models\RegisterForm;
use app\models\SubscribeBox;
use app\models\User;
use app\models\UserPermissions;
use app\models\Users;
use Carbon\Carbon;
use Yii;
use yii\base\BaseObject;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\helpers\Json;
use app\components\PhpList;

use muzna\contact\models\ContactForm;
use muzna\contact\models\Contact;


use function Symfony\Component\Translation\t;

class ContactController extends Controller
{

    public $layout = 'main-contact';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
//                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionContacta()
    {
        return $this->render('contact');
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        //$this->layout =  '';
        $model = new Contact();

            if ($this->request->isPost && Yii::$app->request->isAjax) {
                $newId = Contact::find()->max('id') + 1;
                $model->id= $newId;
                $model->load(Yii::$app->request->post()) ;

                $model->verifyCode = $this->request->post('g-recaptcha-response');
               // $model->contact('info@me.com');
                    if ($model->save()) {
                        $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        $result = ['status' => 'success',
                            'title' => Yii::t('cpm', 'success!'),
                            'msg' => Yii::t('cpm', 'your message sent successfully'),
                            'confirm' => Yii::t('cpm', 'ok')];
                        return $result;
                    } else {
                        $result = Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                        $result = ['status' => 'error',
                            'title' => Yii::t('cpm', 'error!'),
                            'msg' => Yii::t('cpm', 'some thing went wrong'),
                            'confirm' => Yii::t('cpm', 'ok'),
                            'err'=>$model->getErrors()];

                        return $result;
                    }
                }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }



}

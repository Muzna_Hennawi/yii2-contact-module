<?php

namespace muzna\contact;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * contact module definition class
 */
class Contact extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'muzna\contact\controllers';


    /**
     * {@inheritdoc}
     */
    public function init()
    {

        parent::init();
        Yii::setAlias('@contact', static::getBaseDir());
        $config = require(__DIR__ . '/config/web.php');
        // custom initialization code goes here
        self::registerTranslations();
        \Yii::configure($this, $config);
    }
    public static function getBaseDir()
    {
        return __DIR__;
    }



    public function registerTranslations()
    {
        //This registers translations for the Contact Us module /
    Yii::$app->i18n->translations['app*'] = [
        'class' => 'yii\i18n\PhpMessageSource',
        'basePath' => '@contact/messages',
        'sourceLanguage' => 'ar',
        'fileMap' => [
            'app' => 'app.php',
            'app/error' => 'error.php',
        ],
    ];
    }

}

$(document).ready(function() {
    $("#contact-form").on("beforeSubmit", function () {
        var createUrl = $("#contact-btn").attr("data-url");
        var dataForm = $("#contact-form").serialize();

        $.ajax({
            url: createUrl,
            type: "POST",
            data: dataForm,
            success: function (data) {
                if (data.status === "success") {
                    alert(data.msg);
                  /*  Swal.fire({
                        title: data.title,
                        text: data.msg,
                        icon: 'success',
                        confirmButtonText: data.confirm,
                        showCloseButton: false,
                        showConfirmButton:false,
                        timer:2000,
                        customClass: "swal-wide",
                    });*/
                } else if (data.status === "error") {
                    alert(data.err.verifyCode[0]);
                  /*  Swal.fire({
                        title: data.title,
                        text: data.err.verifyCode[0],
                        icon: "warning",
                        confirmButtonText: data.confirm,
                        showCloseButton: false,
                        showConfirmButton:false,
                        timer:2000,
                        customClass: "swal-wide",
                    });*/
                }  else {
                    alert('error');
                   /* Swal.fire({
                        title: data.title,
                        text: data.msg,
                        icon: "warning",
                        confirmButtonText: data.confirm,
                        showCloseButton: false,
                        showConfirmButton:false,
                        timer:2000,
                        customClass: "swal-wide",
                    });*/
                }
                return false;
            },
            error: function () {
                alert('error');
             /*   Swal.fire({
                    title: 'error',
                    text: 'error',
                    icon: "warning",
                    confirmButtonText: 'ok',
                    showCloseButton: true,
                    customClass: "swal-wide",
                });*/
            },

        });
        return false;
    }).on("submit", function (e) {
        e.preventDefault();
        return false;

    });

});
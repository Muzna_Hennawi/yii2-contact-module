<?php

namespace muzna\contact\components;

use app\models\AnnUsers;
use app\models\User;
use app\models\Users;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Created by JetBrains PhpStorm.
 * User: Ahmad
 * Date: 12/30/13
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
class HelperM extends \yii\base\Component
{
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 0;
    const GENDER_UNKNOWN = 2;


    public static function getGenderList(){
        return [
            self::GENDER_FEMALE => Yii::t('cpm', 'Female'),
            self::GENDER_MALE => Yii::t('cpm', 'Male'),
            self::GENDER_UNKNOWN => Yii::t('cpm', 'Unknown'),

        ];

    }
    public static  function getGenderValue($value){

        return ArrayHelper::getValue(self::getGenderList(),$value);
    }

    public static function getLinkDomain($link){
        $urlParts = parse_url($link);
        $domain = preg_replace('/^www\./', 'http://', $urlParts['host']);
        //$domain=$urlParts['host'];
        return $domain;
    }



    public static function cleanUrl($url_slug)
    {
        $array = [' ', '  ', ' - ', '--', '---'];
        $punctuationSymbols = ['.', ',', '/', '$', '*', ':', ';', '!', '?', '|', '\\', '_', '<', '>', '#', '~', '"',
            '\'', '^', '(', ')', '=', '+', '”', '“', ']', '[', '}', '{', '@', '%', '&', 'َ', 'ً', 'ُ', 'ٌ', 'ِ', 'ٍ', 'ْ', '’',
            'ّ', '،', '؛', '«', '»', '☆', '¤', '؟'];

        $res = trim(str_replace($punctuationSymbols, '', $url_slug));
        $res = str_replace($array, '-', $res);
        return $res;
    }

}